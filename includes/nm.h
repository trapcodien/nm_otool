/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/15 13:46:54 by garm              #+#    #+#             */
/*   Updated: 2015/03/17 02:10:40 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NM_H
# define NM_H

/* TEMP */
# include <stdio.h>

# include <sys/mman.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include "libft.h"

typedef struct						s_file
{
	void							*ptr;
	size_t							size;
}									t_file;

typedef struct mach_header_64		t_header_64;
typedef struct mach_header			t_header;
typedef struct load_command			t_loader;
typedef struct symtab_command		t_symtab;
typedef struct segment_command_64	t_segment_64;
typedef struct segment_command		t_segment;
typedef struct section_64			t_section_64;
typedef struct section				t_section;
typedef struct nlist_64				t_nlist_64;
typedef struct nlist				t_nlist;

/*
** debug.c
*/
void			print_header(t_header *header);
void			print_loader(t_header *h, t_loader *loader);

void			print_segment_64(t_segment_64 *segment);
void			print_section_64(t_section_64 *section);

void			print_symtab(t_header *h, t_symtab *symtab);
void			print_nlist_64(t_nlist_64 *symbol, char *stringtable);


#endif
