/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 19:59:28 by garm              #+#    #+#             */
/*   Updated: 2014/06/21 18:43:07 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(const char *s, unsigned int start, size_t len)
{
	char	*fresh_str;
	size_t	i;

	fresh_str = ft_strnew(len);
	if (!fresh_str)
		return (NULL);
	i = 0;
	while (i < len)
	{
		fresh_str[i] = s[start + i];
		i++;
	}
	return (fresh_str);
}
