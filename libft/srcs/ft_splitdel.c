/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_splitdel.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/18 22:56:01 by garm              #+#    #+#             */
/*   Updated: 2014/06/18 23:05:39 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_splitdel(char ***split)
{
	size_t		i;

	if (!split || !*split)
		return ;
	i = 0;
	while ((*split)[i])
	{
		ft_strdel(&((*split)[i]));
		i++;
	}
	ft_memdel((void *)split);
}
