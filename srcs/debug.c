/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/16 22:50:59 by garm              #+#    #+#             */
/*   Updated: 2015/03/17 02:31:06 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

void			print_header(t_header *header)
{
	if (header->magic == MH_MAGIC_64)
		ft_putendl("------------ HEADER (64 bits) ------------");
	else if (header->magic == MH_MAGIC)
		ft_putendl("------------ HEADER (32 bits) ------------");
	if (header->filetype == MH_OBJECT)
		ft_putendl("Filetype : Object");
	else if (header->filetype == MH_EXECUTE)
		ft_putendl("Filetype : Executable");
	ft_printf("Number of cmds : %i (%i bytes)\n\n", header->ncmds, header->sizeofcmds);
}

void			print_section_64(t_section_64 *section)
{
	ft_putendl("\t\t\t--- SECTION_64 ---");
	ft_printf("\t\t\tSection name: %s\n", section->sectname);
	ft_printf("\t\t\tSegment name: %s\n", section->segname);
	printf("\t\t\tMemory addr: %p\n", (void *)section->addr);
	printf("\t\t\tMemory size: %lu\n", (unsigned long )section->size);
	printf("\t\t\tFile offset: %p\n", (void *)(long)section->offset);
	ft_putendl(NULL);
}

void			print_segment_64(t_segment_64 *segment)
{
	unsigned int	i;
	t_section_64	*section;

	ft_putendl("\t--- SEGMENT_64 ---");
	ft_printf("\tSegment size: %i\n", segment->cmdsize);
	ft_printf("\tSegment name: %s\n", segment->segname);
	printf("\tMemory addr: %p\n", (void *)segment->vmaddr);
	printf("\tMemory size: %lu\n", (unsigned long)segment->vmsize);
	printf("\tFile offset: %p\n", (void *)segment->fileoff);
	printf("\tSize in file: %lu\n", (unsigned long) segment->filesize);
	printf("\tNumber of sections: %i\n", segment->nsects);
	i = 0;
	section = (t_section_64 *)(segment + 1);
	while (i < segment->nsects)
	{
		print_section_64(section);
		section++;
		i++;
	}
}

void			print_symtab(t_header *h, t_symtab *symtab)
{
	unsigned int	i;
	t_nlist_64		*symbol;

	ft_putendl("\t--- SYMTAB ---");
	ft_printf("\tSymtab_command size: %i\n", symtab->cmdsize);
	printf("\tSymbole table : %p\n", (void *)(long)symtab->symoff);
	printf("\tNumber of symbols : %i\n", symtab->nsyms);
	printf("\tString table : %p\n", (void *)(long)symtab->stroff);
	printf("\tString table size : %i\n", symtab->strsize);
	i = 0;
	symbol = (t_nlist_64 *)((char *)h + symtab->symoff);
	while (i < symtab->nsyms)
	{
		print_nlist_64(symbol, ((char *)h + symtab->stroff));
		symbol++;
		i++;
	}
}

void			print_nlist_64(t_nlist_64 *symbol, char *stringtable)
{
	ft_putendl("\t\t\t--- SYMBOL_64 (nlist) ---");
	
	printf("\t\t\tString table index : %i\n", symbol->n_un.n_strx);
	printf("\t\t\tAddr (n_value) : %p\n", (void *)symbol->n_value);
	printf("\t\t\tString : %s\n", stringtable + symbol->n_un.n_strx);
	printf("\t\t\tsect : %i\n", symbol->n_sect);
	ft_putendl(NULL);
}

void			print_loader(t_header *h, t_loader *loader)
{
	ft_putendl("------------ LOAD_COMMAND ------------");
	if (loader->cmd == LC_SEGMENT_64)
		print_segment_64((t_segment_64 *)loader);
	else if (loader->cmd == LC_SYMTAB)
		print_symtab(h, (t_symtab *)loader);
	else
		ft_printf("type: unknow (%i)\n", loader->cmd);
}
