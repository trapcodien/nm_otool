/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/15 13:49:40 by garm              #+#    #+#             */
/*   Updated: 2015/03/17 02:06:00 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"


t_file		load_file(const char *path)
{
	int				fd;
	struct stat		file_stat;
	t_file			file_infos;

	file_infos.ptr = MAP_FAILED;
	file_infos.size = 0;
	if ((fd = open(path, O_RDONLY)) == -1)
		return (file_infos);
	if (fstat(fd, &file_stat) != 0)
	{
		close(fd);
		return (file_infos);
	}
	file_infos.size = file_stat.st_size;
	file_infos.ptr = mmap(NULL, file_infos.size, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);
	return (file_infos);
}

int			unload_file(t_file file_infos)
{
	if (file_infos.ptr == MAP_FAILED || file_infos.size == 0)
		return (0);
	if (munmap(file_infos.ptr, file_infos.size) != 0)
		return (0);
	return (1);
}

/*
int			play_with_file(t_file f)
{
	t_mach_header_64		*head;
	t_load_command			*loader;
	t_segment_command_64	*seg_loader;
	t_section_64			*section;
	t_symtab_command		*symtab;
	char					*string_table;

	head = f.ptr;
	ft_printf("SIZEOF COMMANDS : %i\n", head->sizeofcmds);
	if (head->magic == MH_MAGIC_64)
		ft_putendl("64 bits.");
	else if (head->magic == MH_MAGIC)
		ft_putendl("32 bits.");
	else
	{
		ft_putendl("ERROR : Unknow Magic code.");
		return (1);
	}


	if (head->filetype == MH_EXECUTE)
		ft_putendl("Is an executable.");

	loader = (t_load_command *)(head + 1);

	unsigned int i = 0;
	while (i < head->ncmds)
	{
		printf("Loader %i : CMD 0x%x (%i bytes).\n", i, loader->cmd, loader->cmdsize);
		if (loader->cmd == LC_SEGMENT_64)
		{
			seg_loader = (t_segment_command_64 *) loader;
			if (!ft_strncmp(seg_loader->segname, "__TEXT", 6))
			{
				ft_putendl(seg_loader->segname);
				ft_printf("OFFSET : %i\n", seg_loader->fileoff);
				ft_printf("SIZE : %i\n", seg_loader->filesize);
				ft_printf("Sections : %i\n", seg_loader->nsects);

				section = (t_section_64 *) (seg_loader + 1);
				section += 3;
				ft_putendl(section->segname);
				ft_putendl(section->sectname);
				ft_printf("ADDR : %i ||| OFFSET : %i ||| SIZE : %i\n", section->addr, section->offset, section->size);
				ft_printf("TEST : %i\n", section->reserved2);
			}

		}
		if (loader->cmd == LC_SYMTAB)
		{
			ft_putendl("SYMTAB FOUND");
			symtab = (t_symtab_command *) loader;
			ft_printf("NSYMS : %i\n", symtab->nsyms);
			ft_printf("stroff : %i\n", symtab->stroff);
			ft_printf("strsize : %i\n", symtab->strsize);
			string_table = (char *)head + symtab->stroff;
			string_table += 2;
			ft_putendl(string_table);
		}
		i++;
		loader = (t_load_command *)((char *) loader + loader->cmdsize);
	}



	return (0);
}
*/

int			do_stuff32(t_file f)
{
	t_header		*header;

	header = f.ptr;
	print_header(header);
	return (0);
}

int			do_stuff64(t_file f)
{
	t_header_64		*header;
	t_loader		*loader;
	unsigned int	i;

	header = f.ptr;
	loader = (t_loader *)(header + 1);
	print_header((t_header *)header);
	i = 0;
	while (i < header->ncmds)
	{
		if (loader->cmd == LC_SEGMENT_64 || loader->cmd == LC_SYMTAB)
		{
			print_loader((t_header *)header, loader);
			ft_putchar('\n');
		}
		loader = (t_loader *)((char *)loader + loader->cmdsize);
		i++;
	}

	return (0);
}

int			main(int argc, char **argv)
{
	t_file		file_infos;
	int			error = 0;
	uint32_t	*magic;

	if (argc != 2)
	{
		ft_fprintf(2, "usage: %s <file>\n", argv[0]);
		return (1);
	}
	file_infos = load_file(argv[1]);
	if (file_infos.ptr == MAP_FAILED)
	{
		ft_putendl_fd("ERROR : Unable to map file.", 2);
		return (1);
	}
	magic = file_infos.ptr;
	if (*magic == MH_MAGIC)
		error = do_stuff32(file_infos);
	else if (*magic == MH_MAGIC_64)
		error = do_stuff64(file_infos);
	else
		ft_putendl_fd("ERROR : Bad Magic code.", 2);
	unload_file(file_infos);
	return (error);
}
